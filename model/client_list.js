//Importa a classe client que contem metodos e informações relevantes de cada client
const Client = require('./client.js');

//A classe ClientList que lida com o armazenamento dos clientes conectados
class ClientList {
    constructor(){
        this._list = [];
    }

    //Recebe um socket e diciona a lista de cliente e atribui um ID único ao socket
    //Retorna um objeto Client no final
    addClient(socket) {
        var last_id = 0;
        if(this.size() != 0){
            last_id = this._list[this._list.length-1].id;
        }
        
        var client = new Client(socket, last_id+1);
        this._list.push(client);

        return client;
    }

    //Retorna um client da lista pelo ID, caso não exita retorna undefined
    getClient(id) {
        for(var i = 0; i < this._list.length; i++) {
            if(this._list[i].id === id) {
                return _list[i];
            }
        }
        return undefined;
    }

    //Remove um cliente da lista
    removeClient(id) {
        this._list = this._list.filter(function(obj) {
            return obj.id !== id;
        })   
    }

    //Retornar a quatidade de clientes conectado ao servidor IRC
    size() {
        return this._list.length;
    }

    //Envia uma mensagem para todos os clientes conetado ao servidor IRC
    broadcast(msg) {
        this._list.forEach(client => {
            client.write('msg' + '\r\n');
        });
    }

    //Retorna um objeto cliente da lista com base em seu nick
    getClientNick(nick) {
        for(var i = 0; i < this._list.length; i++) {
            if(this._list[i].nick === nick) {
                return this._list[i];
            }
        }
        return undefined;
    }
    

}

module.exports = ClientList;