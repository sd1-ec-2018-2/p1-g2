//Importa a classe channel que contem metodos e informações relevantes de cada channel
const Channel = require('./channel.js');

//A classe ChannelList que lida com o armazenamento dos channels conectados
class ChannelList {
    constructor(){
        this._list = [];
    }

    //Recebe um nome e um objeto Cliente, se não ja existe u canal com este novo cria-se um novo
    joinChannel(name, client) {
        var channel = this.getChannel(name);

        if(!channel) {
            channel = new Channel(name, client);
            this._list.push(channel);
            return true;
        } else {
            return channel.addMember(client);
        }
    }

    //Retorna um channel da lista pelo nome, caso não exita retorna undefined
    getChannel(name) {
        for(var i = 0; i < this._list.length; i++) {
            if(this._list[i].name === name) {
                return this._list[i];
            }
        }
        return undefined;
    }

    //Remove um channel da lista
    removeChannel(name) {
        this._list = this._list.filter(function(obj) {
            return obj.name !== name;
        }); 
    }

    //Deleta channels vazios
    removeEmpty() {
        this._list = this._list.filter(function(obj) {
            return obj.numberOfMembers() !== 0;
        }); 
    }

    //Verifica se existe um channel com esse nome na lista
    existChannel(name) {
        for(var i = 0; i < this._list.length; i++) {
            if(this._list[i].name === name) {
                return true;
            }
        }
        return false;
    }

    //Quantidade de canais existentes
    size() {
        return this._list.length;
    }

    //Manda uma mensagem para todos mundo que está em pelo menos um canal
    broadcast(msg) {
        this._list.forEach(channel => {
            channel.broadcast(msg);
        });
    }

    //Manda um cliente de tods os canais, util para quando o cliente se desconecta do servidor
    removeClientFromAll(id){
        this._list.forEach(channel => {
            channel.removeID(id);
            if(channel.numberOfMembers() === 0){
                this.removeChannel(channel.name);
            }
        });
    }

}

module.exports = ChannelList;