//A classe client que contem metodos e informações relevantes de cada client
//A colocar nick, canais, etc ...

class Client {
    constructor(socket, id){
        this.socket = socket;
        this.id = id;

        this.password = undefined

        // NICK x
        this.nick = '*';

        //USER username usermode *: realname
        this.username = '*';
        this.realname = undefined
        this.usermode = undefined;

        //True caso ja tenha execultado ambas funções USER e NICK
        this.logged = false;

        //Lista de Canais em que cliente está logado (É controlado pela classe Channels)
        this.channelList = [];

        this.serverName = '';
        this.iplocal = socket.address().address.split(':')[3];
        this.connected = true;

        this.modeList = [];
        this.quitted = false;
        this.active = true;
    }

    //Verifica se um cliente já forneceu informações suficientes para ser logado no servidor
    canBeLogged() {
        if(this.nick !== '*' && this.username !== '*' &&
            this.usermode !== undefined && this.realname !== undefined &&
            this.connected) {

            return true;
        }
        return false;
    }

    //Envia uma mensagem para o cliente, adiciona automaticamente a quebra de linha
    write(msg) {
        this.socket.write(msg + '\r\n');
    }

    //Acessa o ultimo canal acesso pelo cliente
    getLastChannel(){
        if(this.channelList.length > 0){
            return this.channelList[this.channelList.length-1].name;
        }else{
            return '';
        }

    }

    //Testa se dois clientes diferentes possuem pelo menos 1 canal em comum
    hasSameChannels(client){
        var channelListExternal = client.channelList;
        for(var i=0;i<channelListExternal.length;i++){
            for(var j=0;j<this.channelList.length;j++){
                if(channelListExternal[i].nick === this.channelList[j].nick){
                    return true;
                }
            }
        }
        return false;
    }

    sendBroadcastForAllChannels(msg) {
        this.channelList.forEach((channel) => {
            channel.broadcast(msg);
        });
    }

    addMode(mode_tag) {
        if(this.modeList.includes(mode_tag)){
            return false;
        }
        this.modeList.push(mode_tag);
        return true;
    }

    removeMode(mode_tag) {
        var old_legnth = this.modeList.length;
        this.modeList = this.modeList.filter(function (mode) {
            return mode !== mode_tag
        });
        return old_legnth !== this.modeList.length;
    }

    getChannelsNames() {
        var channels_names = [];
        this.channelList.forEach(channel => {
            channels_names.push('@'+channel.name);
        });
        return channels_names;
    }
}

module.exports = Client;
