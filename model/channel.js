//A classe chanel possui tdos os métodos relevantes do canal

class Channel {
    constructor(name, creator){
        this.name = name;
        //Criador do canal / Admin?
        this.creator = creator;
        //Lista de clientes conectado ao canal
        this.userList = [];

        //Adciona o criador do canal a lista de membros automaticamente
        this.addMember(creator);
        this.topic = undefined;

        this.modeList = ['n','t'];
    }

    //Retorna a quantidade membros que o canal possui
    numberOfMembers() {
        return this.userList.length;
    }

    //Adiciona um membro ao canal a partir do objeto Cliente
    //retorna true se o membro foi adicionado com sucesso e false se o membro já estava dentro canal
    addMember(client) {
        if(!this.hasMember(client.nick)){
            this.userList.push(client);
            //Também atulizada a lista de canais dentro do proprio cliente
            client.channelList.push(this);
            return true;
        } else {
            return false;
        }
    }

    //Remove um cliente do canal com base em seu nick
    removeMember(nick) {
        if(!this.hasMember(nick)) return false;
        
        var self = this;
        this.userList = this.userList.filter(function(obj) {
            if(obj.nick === nick) {
                //Remove o canal da lista de canais que fica dentro do objeto cliente
                obj.channelList = obj.channelList.filter(function(obj2) {
                    return obj2.name !== self.name;
                });
            }
            return obj.nick !== nick;
        });
        return true;  
    }

    //Remove um cliente do canal com base em seu id
    removeID(id) {
        var self = this;
        this.userList = this.userList.filter(function(obj) {
            if(obj.id === id) {
                obj.channelList = obj.channelList.filter(function(obj2) {
                    return obj2.name !== self.name;
                });
            }
            return obj.id !== id;
        });
    }

    //Retorna true se existir um cliente com certo nick dentro do canal
    hasMember(nick) {
        for(var i = 0; i < this.userList.length; i++) {
            if(this.userList[i].nick === nick) {
                return true;
            }
        }
        return false; 
    }

    //Envia uma mensagem para todos os clientes do canal
    broadcast(msg) {
        this.userList.forEach(client => {
            client.socket.write(msg + '\r\n');
        });
    }

    broadcastExcept(msg, currentClient) {
        this.userList.forEach(client => {
            if(currentClient.id != client.id){
                client.socket.write(msg + '\r\n');
            } 
        });
    }

    nickList(){
        var nickArray = [];
        ///..
        for(var i = 0; i < this.userList.length; i++)
        {
            var nickname = this.userList[i].nick;
            if(nickname === this.creator.nick) {
                nickname = '@'+nickname;
            } 
            nickArray.push(nickname);
        }
        return nickArray;
    }

    addMode(mode_tag) {
        if(this.modeList.includes(mode_tag)){
            return false;
        }
        this.modeList.push(mode_tag);
        return true;
    }

    removeMode(mode_tag) {
        var old_legnth = this.modeList.length;
        this.modeList = this.modeList.filter(function (mode) {
            return mode !== mode_tag
        });
        return old_legnth !== this.modeList.length;
    }

}

module.exports = Channel;