//Principal objeto que irá interpretar as resposta do usuario e exercutar as ações necessárias
const irc = new Object();
const fs = require('fs');

//Cada função implementa uma funcionalidade com o mesmo nome:
//o parser() é responsavel por fazer o mapeamento e executar as funções

//Variáveis Globais
var servername = 'g2irc.ufg.local'
irc.servername = servername
irc.serverversion = 'g2irc-1:1.0.0-(20180925)'
irc.nickLen = 15
irc.startTime = (new Date()).getTime()
var motdPath = 'config/messageOfTheDay.motd'
irc.conections = 0;

//Interpreta a mensagem do enviada pelo cliente e manda a resposta adequada
//O irc.listen atribui a função ao objeto irc para ser exportada em outros arquivos
irc.listen = function(data, currentClient, clientList, channelList){
    //Cada comando recebido deve ser separado por uma quebra de linha
    //O replace '\r' se da porque alguns sistemas, como windows, tem um quebra de linha do tipo '\r\n' e outros ñ
    input_list = data.replace('\r', '').split('\n');

    //Para cada input invoca o parser
    input_list.forEach(input => {
        parser(input, currentClient, clientList, channelList);
    });
}

//Responsavel procesar cada um dos comandos enviados
function parser(data, currentClient, clientList, channelList) {
    //Separa o camando em uma array pelo seus espaços, EX:
    //"USER edu 0 *: Eduardo"
    // =>
    //['USER', 'edu', '0', '*:', 'Eduardo']
    var data_array = data.split(' ');
    //O comando sempre vai estar no primeiro elemento do array
    var command = data_array[0].toUpperCase();
    
    var allowedCommands = ['NICK', 'USER', 'PASS', 'QUIT','JOIN', 'PART', 'MODE', 'CHANNEL', 'TOPIC', 'NAMES', 'LIST', 'PRIVMSG', 'NOTICE', 'MOTD', 'LUSERS', 'VERSION', 'STATS', 'TIME', 'WHO', 'WHOIS', 'KILL', 'PING', 'PONG']

    var notLogged_allowedCommands = ['NICK', 'USER', 'PASS', 'QUIT', 'PING', 'PONG']
    

    if(!currentClient.logged) {
        if(!allowedCommands.includes(command)){
            return;
        }
        if(!notLogged_allowedCommands.includes(command)){
            sendErrorMessage(451, currentClient);
            return;
        }
    } else {
        if(!allowedCommands.includes(command)){
            serverWriteMsg('Unknown command', currentClient, '421', ' '+command);
            return;
        }
    }

    //Esse if serve para caso o cliente mande uma string vazia....
    if(command) {
        try {
            //eval executa uma string como um codigo javascript
            //Então por exemplo, se command = 'NICK' o código abaixo será traduzido para invocação de função:
            //NICK(data_array, currentClient, clientList, channelList);
            eval(command)(data_array, currentClient, clientList, channelList);
        } catch(err) {
            //Caso o comando seja invalido (Não tenha uma função implementada), ele joga uma exceção
            //A mensagem de erro só é levada ao cliente caso ele esteja logado
            if(err instanceof ReferenceError){
                if(currentClient.logged){
                    console.log(err)
                    serverWriteMsg('Unknown command', currentClient, '421', ' '+command);
                } else {
                    console.log(err);
                }
            } else {
                console.log(err);
            }
        }
    }

}

/////////////////////////////////////////////////////////////////////
/////////////////////FUNÇÕES DE ENVIO DE MENSAGENS///////////////////
/////////////////////////////////////////////////////////////////////

//São funções auxiliares que ajudam em um formato padrão de resposta do servidor.
//Não é obrigatorio usa-lás já que muitas respostas tem padrões difentes e fica complicado codificar tudo em função só
//mas talvez possam ajudar

//COnjuntos de respostas Padrões do servidor
//Usado multiplas vezes ao longo código
function serverReponse(currentClient, clientList, type, data) {
    switch(type) {
        case 'logged':
            serverWriteMsg('Logado com sucesso', currentClient, 'NOTICE');
            serverWriteMsg(`Existem ${clientList.size()} usuarios logados`, currentClient, '251');
            MOTD(currentClient);
            clientWriteMsg('+i', currentClient, `MODE ${currentClient.nick}`);
            break;
        default:
            serverWriteMsg(msg, currentClient, 'NOTICE');
    }
}

//Envia uma mensagem ao cliente com o cabeçalho do servidor, do tipo:
//Ex:
//:g2irc.ufg.local 375 edu :- g2irc.ufg.local Message of the Day -
function serverWriteMsg(data, client, header, extras) {
    var premsg;
    if(extras) {
        premsg = `:${servername} ${header} ${client.nick} ${extras} :`;
    } else {
        premsg = `:${servername} ${header} ${client.nick} :`;
    }

    client.write(premsg + data);
}
irc.serverWriteMsg = serverWriteMsg;

//Envia uma mensagem ao cliente com o cabeçalho do usuario, do tipo:
//Ex:
//:g2irc.ufg.local 375 edu :- g2irc.ufg.local Message of the Day -
function clientWriteMsg(data, client, extras) {
    var premsg = `:${client.nick}!${client.username}@${client.iplocal} ${extras} :`;

    client.write(premsg + data);
}


////////////////////////////////////////////////////////////////////////
////////////////////////FUNCIONALIDADES DO SISTEMA//////////////////////
////////////////////////////////////////////////////////////////////////

// Cada comando deve ser escrito em sua função idepedente que começa com letra maiscula e redebe os seguintes dados:
// function COMANDO(data_array, currentClient, clientList, channelList) {}
// ESSE PADRÃO DEVE SER SEGUIDO POIS O PARSER EXECUTA A FUNÇÃO DE ACORDO COM SEU NOME E O COMANDO RECEBIDO

// Variáveis:
// - data_array: Um array do comando recebido pelo servidor que foi quebrado a partir de seus espaços
//                      Ex: ['USER', 'edu', '0', '*:', 'Eduardo']
// - currentClient: objeto Cliente que chamou o comando, é definido pela classe '../model/client.js'
// - clientList: objeto Lista que contem todos os cliente conectados ao servidor, é definido pela classe '../model/client_list.js'
// - channelList: objeto Lista que contem todos os canais existestes no servidor, é definido pela classe '../model/channel_list.js'

//PS: LEIAM AS CLASSES PARA ENTENDEREM OS OBJETOS RECEBIDOS.

function NICK(data_array, currentClient, clientList, channelList){
    var nick = data_array[1];
    if(!nick) {
        sendErrorMessage(431, currentClient);
        return;
    }
    //TODO: REGEX DE nickname
    if(nick == '*') {
        sendErrorMessage(432, currentClient, nick);
        return;
    }
    if(clientList.getClientNick(nick)) {
        sendErrorMessage(433, currentClient, nick);
        return;
    }
    //ERR_NICKCOLLISION não implementado por configuração multi host nao feita
    //ERR_UNAVAILRESOURCE nick delay????
    //ERR_RESTRICTED para user mode resistricted(+r)

    currentClient.nick = nick;

    if(!currentClient.logged){
        if(currentClient.canBeLogged()){
            currentClient.logged = true;
            currentClient.serverName = servername;
            serverReponse(currentClient, clientList, 'logged');
        }
    } else {
        clientWriteMsg(nick, currentClient, 'NICK');
    }
}

function USER(data_array, currentClient, clientList, channelList){

    if(!data_array[1] || !data_array[2] || !data_array[3] || !data_array[4]){
        sendErrorMessage(461, currentClient, 'USER');
        return;
    }

    //Verifica se é do tipo '*:' ou '* :' para configurar o realname
    if(data_array[3].includes('*:')){
        currentClient.realname = data_array.slice(3).join(' ').replace('*:', '');
    } else if(data_array[3] == '*') {
        currentClient.realname = data_array.slice(4).join(' ').replace(':', '');
    //else para rfc1459
    } else if(data_array.length >= 5) {
        data_array[2] = 0
        currentClient.realname = data_array.slice(5).join(' ').replace(':', '');
    } else {
        sendErrorMessage(461, currentClient, 'USER');
        return;
    }

    currentClient.username = data_array[1];

    currentClient.usermode = data_array[2];
    //TODO: ADICIONAR TRATAMENTO DE USERMODE CORRETO
    currentClient.addMode('i');

    if(!currentClient.logged){
        if(currentClient.canBeLogged()){
            currentClient.logged = true;
            currentClient.serverName = servername;
            serverReponse(currentClient, clientList, 'logged');
        }
    } else {
        sendErrorMessage(462, currentClient);
    }
}

function PASS(data_array, currentClient, clientList, channelList) {
    if(data_array.length <= 1){
        sendErrorMessage(461, currentClient, 'PASS');
        return;
    }
    if(currentClient.logged){
        sendErrorMessage(462, currentClient);
        return;
    }

    currentClient.password = data_array[1];
}

function QUIT(data_array, currentClient, clientList, channelList){
    if(data_array[1]) {
        currentClient.sendBroadcastForAllChannels(`:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} QUIT :${data_array.slice(1).join(' ').slice(1)}`);
    } else {
        currentClient.sendBroadcastForAllChannels(`:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} QUIT :Remote host closed the connection`);
    }
    currentClient.quitted = true;
    currentClient.socket.end();

}

function MODE(data_array, currentClient, clientList, channelList) {
    var n_arguments = data_array.length

    if(n_arguments <= 1) {
        sendErrorMessage(461, currentClient, 'MODE');
        return;
    }

    if(data_array[1].match('#')){
        //Se for mode para channel
        var channel_name = data_array[1]
        var channel = channelList.getChannel(channel_name)

        if(!channel){
            sendErrorMessage(403, currentClient, channel_name);
            return;
        }

        if(n_arguments === 2) {
            var premsg = `:${servername} 324 ${channel_name} +`
            currentClient.write(premsg+channel.modeList.join(''));
            return;
        }

        var mode_param = data_array[2]
        var add_or_sub = mode_param[0]

        if(add_or_sub !== '-' && add_or_sub !== '+'){
            sendErrorMessage(461, currentClient, 'MODE');
            return;
        }

        var mode_list = mode_param.slice(1)
        if(!mode_list){
            sendErrorMessage(461, currentClient, 'MODE');
            return;
        }
        var success = false;
        var supported_modes = ['O','o','v','a','i','m','n','q','p','s','r','t','k','l','b','e','I'];
        mode_list = mode_list.split('');
        var modes_that_worked = []
        mode_list.forEach((mode) => {
            if(!supported_modes.includes(mode)){
                sendErrorMessage(472, currentClient, mode);
                return;
            }
            if(add_or_sub === '+'){
                success = channel.addMode(mode)
            } else {
                success = channel.removeMode(mode)
            }
            if (success) modes_that_worked.push(mode)
        });
        var premsg = `:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} MODE`;
        if(modes_that_worked.length !== 0) channel.broadcast(premsg + ` ${channel_name} ${add_or_sub}${modes_that_worked.join('')}`);
    } else {
        //Se for modo para nick
        var nick = data_array[1]
        if(nick != currentClient.nick){
            sendErrorMessage(502, currentClient);
            return;
        }
        if(n_arguments === 2) {
            var premsg = `:${servername} 221 ${currentClient.nick} +`
            currentClient.write(premsg+currentClient.modeList.join(''));
            return;
        }

        var mode_param = data_array[2]
        var add_or_sub = mode_param[0]

        if(add_or_sub !== '-' && add_or_sub !== '+'){
            sendErrorMessage(461, currentClient, 'MODE');
            return;
        }

        var mode_list = mode_param.slice(1)
        if(!mode_list){
            sendErrorMessage(461, currentClient, 'MODE');
            return;
        }
        var success = false;
        var supported_modes = ['a','i','w','r','o','O','s'];
        mode_list = mode_list.split('');
        var modes_that_worked = []
        mode_list.forEach((mode) => {
            if(!supported_modes.includes(mode)){
                sendErrorMessage(501, currentClient);
                return;
            }

            if(add_or_sub === '+'){
                success = currentClient.addMode(mode)
            } else {
                success = currentClient.removeMode(mode)
            }
            if (success) modes_that_worked.push(mode)
        });
        if(modes_that_worked.length !== 0) clientWriteMsg(add_or_sub+modes_that_worked.join(''), currentClient, `MODE ${currentClient.nick}`);
    }

}

function JOIN(data_array, currentClient, clientList, channelList) {
    var n_arguments = data_array.length
    var channel_name;
    if(n_arguments <= 1) {
        sendErrorMessage(461, currentClient, 'JOIN');
        return;
    }

    if(n_arguments === 2) {
        if(data_array[1] == 0) {
           channelList.channelList.forEach((obj) => {
               obj.remove(currentClient.nick);
               currentClient.write(`:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} PART ${obj.name}`);
           });
           channelList.removeEmpty();
        } else {
           var channels = data_array[1].split(',');
           channels.forEach((channel_name) => {
            if(channelList.joinChannel(channel_name, currentClient)) {
                channel =  channelList.getChannel(channel_name);
                channel.broadcast(`:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} JOIN :${channel_name}`);
                if(channel.topic) {
                    serverWriteMsg(channel.topic, currentClient, `${channel.name}`);
                }
                currentClient.write(`:${servername} MODE ${channel_name} +nt`);
                irc.listen('NAMES '+channel_name, currentClient, clientList, channelList);
            }
           });
       }
    }
}

function PART(data_array, currentClient, clientList, channelList) {
    var n_arguments = data_array.length
    if(n_arguments <= 1) {
        sendErrorMessage(461, currentClient, 'PART');
        return;
    }

    if(n_arguments === 2) {
        var channels = data_array[1].split(',');
        channels.forEach((channel_name) => {
            var channel = channelList.getChannel(channel_name)
            if(channel){
                if(!channel.removeMember(currentClient.nick)){
                    sendErrorMessage(442, currentClient, channel_name);
                    return;
                }
                var msgquit = `:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} PART ${channel.name}`;
                channel.broadcast(msgquit)
                currentClient.write(msgquit);
            } else {
                sendErrorMessage(403, currentClient, channel_name);
                return;
            }
        });
        channelList.removeEmpty();
    }
}

function TOPIC(data_array, currentClient, clientList, channelList) {
    var n_arguments = data_array.length
    var channel_name = data_array[1];
    var channel = channelList.getChannel(channel_name)

    if(n_arguments <= 1){
        sendErrorMessage(461, currentClient, 'TOPIC');
        return;
    }

    if(channel) {
        if(!channel.hasMember(currentClient.nick)){
            sendErrorMessage(442, currentClient, channel_name);
            return;
        }
        if(n_arguments === 2) {
            if(!channel.topic) {
                serverWriteMsg('No topic is set', currentClient, `${channel.name}`);
            } else {
                serverWriteMsg(channel.topic, currentClient, `${channel.name}`);
            }
        } else {
            if(!data_array[2].includes(':')){
                sendErrorMessage(461, currentClient, 'TOPIC');
                return;
            }
            data_array[2] = data_array[2].replace(':', '')
            topic = data_array.slice(2).join(' ');
            channel.topic = topic;
            var msg = `:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} TOPIC ${channel.name}: ${topic}`;
            channel.broadcast(msg);
        }
    } else {
        sendErrorMessage(403, currentClient, channel_name);
        return;
    }

}

function NAMES(data_array, currentClient, clientList, channelList){
    if(data_array.length === 1){
        currentClient.write(`:${servername} 366 ${currentClient.nick} * :End of /NAMES list.`);
        return;
    }
    var channel_list = data_array[1].split(',')

    for(var i=0; i<channel_list.length; i++){
        var channelName = channel_list[i];
        var channel =channelList.getChannel(channelName);
        if (channel === undefined){
            sendErrorMessage(402, currentClient);
            return;
        }

        currentClient.write( `:${servername} 353 ${currentClient.nick} = ${channelName} :${channel.nickList().join(' ')}`);
        currentClient.write(`:${servername} 366 ${currentClient.nick} ${channelName} :End of /NAMES list.`);
    }

}

function LIST(data_array, currentClient, clientList, channelList){
    var channel_list = [];

    if(data_array.length === 1){
        channel_list = channelList._list;
    } else {
        data_array[1].split(',').forEach((channel_name) => {
            channel_list.push(channelList.getChannel(channel_name));
        });
    }

    currentClient.write(`:${servername} 321 ${currentClient.nick} Channel :Users  Name`);
    channel_list.forEach((channel) => {
        if(channel){
            currentClient.write(`:${servername} 322 ${currentClient.nick} ${channel.name} ${channel.numberOfMembers()} :[+${channel.modeList.join('')}] ${channel.topic}`);
        }
    });
    currentClient.write(`:${servername} 323 ${currentClient.nick} :End of /LIST`);

}

function PRIVMSG(data_array, currentClient, clientList, channelList){
    var target = data_array[1];
    if(!target){
        sendErrorMessage(411, currentClient);
        return;
    }
    if(!data_array[2] || data_array[2][0] !== ':') {
        sendErrorMessage(412, currentClient);
        return;
    }
    data_array[2] = data_array[2].replace(':', '');
    var msg = data_array.slice(2).join(' ');
    var premsg = `:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} PRIVMSG ${target} :`;

    if(target[0] === '#') {
        var channel = channelList.getChannel(target);
        if(!channel) {
            sendErrorMessage(401, currentClient, target);
            return;
        }
        if(!channel.hasMember(currentClient.nick)){
            sendErrorMessage(404, currentClient, target);
            return;
        }
        channel.broadcastExcept(premsg + msg, currentClient);
    } else {
        var nickname = target.split('@')[0];
        var client = clientList.getClientNick(nickname);
        if(!client) {
            sendErrorMessage(401, currentClient, target);
            return;
        }
        client.write(premsg + msg);
    }
}

function NOTICE(data_array, currentClient, clientList, channelList){
    var target = data_array[1];
    if(!target){
        sendErrorMessage(411, currentClient);
        return;
    }
    if(!data_array[2] || data_array[2][0] !== ':') {
        sendErrorMessage(412, currentClient);
        return;
    }
    data_array[2] = data_array[2].replace(':', '');
    var msg = data_array.slice(2).join(' ');
    var premsg = `:${currentClient.nick}!${currentClient.username}@${currentClient.iplocal} NOTICE ${target} :`;

    if(target[0] === '#') {
        var channel = channelList.getChannel(target);
        if(!channel) {
            sendErrorMessage(401, currentClient, target);
            return;
        }
        if(!channel.hasMember(currentClient.nick)){
            sendErrorMessage(404, currentClient, target);
            return;
        }
        channel.broadcastExcept(premsg + msg, currentClient);
    } else {
        var nickname = target.split('@')[0];
        var client = clientList.getClientNick(nickname);
        if(!client) {
            sendErrorMessage(401, currentClient, target);
            return;
        }
        client.write(premsg + msg);
    }
}

function MOTD(currentClient){
    try {
        buffer = fs.readFileSync(motdPath, 'utf8');
        var body = buffer.toString();
        body = body.split('\n');
        serverWriteMsg('- '+servername+' Message of the Day -', currentClient, '375');
        for(var i = 0; i<body.length; i++){
            serverWriteMsg(body[i], currentClient, '372');
        }
        serverWriteMsg('End of /MOTD command.', currentClient, '376');
    } catch(err) {
        sendErrorMessage(422, currentClient);
    }
}

// Verifica quais os usuários estão online
// Verifica usuários com mesmo IP
function WHO(data_array, currentClient, clientList, channelList){
    if(data_array[1]){
        var searchClient = clientList.getClientNick(data_array[1]);
        if(searchClient){
            if(currentClient.getLastChannel() !== ''){
                currentClient.write(`:${servername} 352 ${currentClient.nick} ${searchClient.username} ${searchClient.iplocal} ${searchClient.serverName} ${searchClient.nick} H :${searchClient.usermode} ${searchClient.realname} `);
            } else{
                currentClient.write(`:${servername} 352 ${currentClient.nick} ${searchClient.getLastChannel()} ${searchClient.username} ${searchClient.iplocal} ${searchClient.serverName} ${searchClient.nick} H@ :${searchClient.usermode} ${searchClient.realname} `);
            }
            serverWriteMsg('End of /WHO list.', currentClient, '315', data_array[1]);
        }else{
            serverWriteMsg('End of /WHO list.', currentClient, '315', data_array[1]);
        }
    }else{
        //lista de who
        arrayClient = clientList._list;
        for(var i=0; i<arrayClient.length; i++){
            if(currentClient.hasSameChannels(arrayClient[i])){
                var searchClient = arrayClient[i];
                currentClient.write(`:${servername} 352 ${currentClient.nick} ${searchClient.username} ${searchClient.iplocal} ${searchClient.serverName} ${searchClient.nick} H :${searchClient.usermode} ${searchClient.realname} `);
            }
        }
        serverWriteMsg('End of /WHO list.', currentClient, '315');
    }
}
//Verifição de usuarios online no servidor
function LUSERS(data_array, currentClient, clientList, channelList){
    currentClient.write(`:${servername} 254 ${currentClient.nick} ${channelList.size()} :channels formed` );
    currentClient.write(`:${servername} 255 ${currentClient.nick} :I have ${clientList.size()} clients and 0 servers`);
    currentClient.write(`:${servername} 265 ${currentClient.nick} :Current local users: ${clientList.size()}`);
    currentClient.write(`:${servername} 266 ${currentClient.nick} :Current global users: ${clientList.size()}`);
}

//Mostra a versão do servidor com os parâmetros aceitos
function VERSION(data_array, currentClient, clientList, channelList){
    currentClient.write(`:${servername} 351 ${currentClient.nick} ${irc.serverversion} :TStow` );
    currentClient.write(`:${servername} 005 ${currentClient.nick} CALLERID CASEMAPPING=utf8 DEAF=D KICKLEN=180 MODES=6 PREFIX=(ohv)@%+ STATUSMSG=@%+ EXCEPTS INVEX NICKLEN=15 NETWORK=debian MAXLIST=beI:100 MAXTARGETS=4 :são suportados por esse servidor` );
    currentClient.write(`:${servername} 005 ${currentClient.nick} CHANTYPES=# CHANLIMIT=#:25 CHANNELLEN=50 TOPICLEN=300 CHANMODES=beI,k,l,cimnprstCMORST ELIST=CMNTU SAFELIST WATCH=50 AWAYLEN=180 KNOCK :são suportados por esse servidor` );
}


function STATS(data_array, currentClient, clientList, channelList){
    if(data_array[1]=='u'){
        var tm = (new Date()).getTime()-irc.startTime
        var day = Math.floor(tm / 1000 / 60 / 60 / 24);
        var hours = Math.floor(tm / 1000 / 60 / 60);
        var minutes = Math.floor(tm / 60000) % 60;
        var seconds =  Math.floor(((tm / 1000) % 60));
        currentClient.write(`:${servername} 242 ${currentClient.nick} Server Up ${day} days, ${hours}:${minutes}:${seconds}` );
        currentClient.write(`:${servername} 250 ${currentClient.nick} :Highest connection count: ${clientList.size()} (${clientList.size()} clients) (${irc.conections++} connections received)` );
        currentClient.write(`:${servername} 219 ${currentClient.nick} u :End of /STATS report` );
    }else{
        sendErrorMessage(461, currentClient);
        return;
    }
}

function TIME(data_array, currentClient, clientList, channelList){
    if(data_array[1] == 'g2irc.ufg.local' || !data_array[1]){
        currentClient.write(`:${servername} 391 ${currentClient.nick} :${(new Date()).toString()}` );
    }else{
        sendErrorMessage(461, currentClient);
        return;
    }
    
}

function WHOIS(data_array, currentClient, clientList, channelList){
    var user = clientList.getClientNick(data_array[1]);

    if(data_array[1]){
        currentClient.write(`:${servername} 311 ${currentClient.nick} ${currentClient.nick}  ${user.username} ${currentClient.iplocal} * :${currentClient.realname}` );
        currentClient.write(`:${servername} 319 ${currentClient.nick} ${currentClient.nick}  :${user.getChannelsNames().join(' ')}`);
        currentClient.write(`:${servername} 319 ${currentClient.nick} ${currentClient.nick}  :is using modes +i`);
        currentClient.write(`:${servername} 319 ${currentClient.nick} ${currentClient.nick}  :End of /WHOIS list.`);
    }else{
        sendErrorMessage(401, currentClient);
    }
    
}
function PING(data_array, currentClient, clientList, channelList){
    // Verifica se foi passado parâmetro
    if(typeof data_array[1] === 'undefined'){
        sendErrorMessage(461,currentClient)
    }
    // Responde ping no servidor
    else if(data_array[1] == servername){
        currentClient.write(`PONG ${servername} :${servername}`);
    }
    // Procura o cliente pingado
    else{
        var searchClient = clientList.getClientNick(data_array[1]);
        if(searchClient){
          // Repassa a mensagem de ping ao cliente
          searchClient.write(`PING ${servername} : ${currentClient.nick}`);
        }else{
            currentClient.write(`${servername}: ${data_array[1]} not connected`);
        }
    }
}

function PONG(data_array, currentClient, clientList, channelList){
    // Verifica se foi passado parâmetro
    if(typeof data_array[1] === 'undefined'){
        sendErrorMessage(461,currentClient)
    }
    // Configura atributo active do cliente para evitar desconexçao por inatividade
    else if(data_array[1] == servername){
        currentClient.active = true;
    }
    // Procura e responde ping em cliente
    else{
        var searchClient = clientList.getClientNick(data_array[1]);
        if(searchClient){
          searchClient.write(`PONG ${servername} : ${currentClient.nick}`);
        }else{
            currentClient.write(`${servername}: ${data_array[1]} not connected`);
        }
    }
}

function KILL(data_array, currentClient, clientList, channelList){
    // Verifica se foi passado parâmetro
    if(typeof data_array[1] === 'undefined'){
        sendErrorMessage(461,currentClient)
    }
    // Procura e disconecta o cliente
    else{
        let message = '';
        var searchClient = clientList.getClientNick(data_array[1]);
        if(searchClient){
            // Mostra a mensagem passada a partir do terceiro parametro do comando
            if(data_array[2] !== undefined){
                message = data_array.slice(2).join(' ');
            }
            searchClient.write(`${servername}: You are being disconneted by "${currentClient.nick}" because "${message}"`)
            searchClient.socket.end();
        }
        else{
            currentClient.write(`${servername}: ${data_array[1]} not found on server`);
        }
    }
}

////////////////////////////////////////////////////////////////////////
////////////////////////MENGAGENS DE ERRO//////////////////////
////////////////////////////////////////////////////////////////////////

function sendErrorMessage(code, currentClient, extra){
    switch(code) {
        case 401:
            serverWriteMsg('No such nick/channel', currentClient, code, extra);
            break;
        case 402:
            serverWriteMsg('No such server', currentClient, code, extra);
            break;
        case 403:
            serverWriteMsg('No such channel', currentClient, code, extra);
            break;
        case 404:
            serverWriteMsg('Cannot send to channel', currentClient, code, extra);
            break;
        case 411:
            serverWriteMsg('No recipient', currentClient, code);
            break;
        case 412:
            serverWriteMsg('No text to send', currentClient, code);
            break;
        case 422:
            serverWriteMsg('MOTD File is missing', currentClient, code);
            break;
        case 431:
            serverWriteMsg('No nickname given', currentClient, code);
            break;
        case 432:
            serverWriteMsg('Erroneus Nickname', currentClient, code, extra);
            break;
        case 433:
            serverWriteMsg('Nickname is already in use', currentClient, code, extra);
            break;
        case 442:
            serverWriteMsg('You\'re not on that channel', currentClient, code, extra);
            break;
        case 451:
            serverWriteMsg('You have not registered', currentClient, code);
            break;
        case 461:
            serverWriteMsg('Not enough parameters', currentClient, code, extra);
            break;
        case 462:
            serverWriteMsg('You may not reregister', currentClient, code);
            break;
        case 472:
            serverWriteMsg('is unknown mode char to me', currentClient, code, extra);
            break;
        case 501:
            serverWriteMsg('Unknown MODE flag', currentClient, code);
            break;
        case 502:
            serverWriteMsg('Cannot change mode for other users', currentClient, code);
            break;
        default:
            serverWriteMsg('Unknowm error message', currentClient, code);
            console.log('Unknowm error message evoked with code: '+ code);
    }
}

// Exporta o objeto irc para ser usado no server.js
module.exports = irc;
