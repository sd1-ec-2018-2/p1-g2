# Projeto 1

## Membros

* Lucas Simões Passos - lucassimoe@gmail.com - Líder
* Bruna Larissa Camara Queiroz - bruna@tudofacil.net - desenvolvedor
* Antonio Igor Rodrigues Dourado - antonio_igorr@hotmail.com - desenvolvedor
* Werikcyano Lima Guimaraes - werikguimaraes2014@hotmail.com - desenvolvedor
* Andre Luiz Cardoso da Costa - mr.luizandre@gmail.com - desenvolvedor
* Eduardo Garcia - edusantosgarcia@gmail.com - desenvolvedor

## Como inciar o servidor

```
node server.js
```

O servidor será iniciado por padrão na porta 6667, para alterar porta altere a variável 'PORT' no arquivo ./server.js
