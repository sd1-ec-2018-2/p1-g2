
# 1. Introdução

O Internet Relay Chat, ou IRC como é mais conhecido, é um serviço da Internet utilizado para teleconferência em modo texto, ou seja, permite aos usuários conversar via mensagens de texto em tempo real.<br>
Ele é constituído de servidores e clientes, de maneira que cada servidor contém informações sobre todo o sistema.<br>
O cliente é implementado em sockets e normalmente utiliza as portas 6666, **6667** ou 6668 para comunicar-se com o servidor. Esta comunicação ocorre sobre TCP/IP, utilizando o protocolo TCP na camada de rede.<br>
Desenvolvido por Jarkko Oikarinen em 1988, o sistema necessitava um servidor ligado 24 horas para manter a comunicação, na época hospedado dentro de Universidades.<br>
O protocolo do Internet Relay Chat é baseado em texto (caracteres ASCII) e está descrito no RFC 1459 ou no RFC 2812.<br>




# 2. Ponderações
## 2.1 Finalidade
O Manual descrito aqui tem finalidade de guiar desenvolvedores de software, analistas e profissionais da tecnlogia em geral, na implementação de um servidor IRC usando o serviço de **sockets**.<br>
## 2.2 Conhecimentos e Habilidades necessárias
Conhecimento técnico em programação, visão sistêmica, habilidade em versionamento de software (git e afins) e noções de rede de computadores <br>

# 3. Estrutura
## 3.1 Protocolo Utilizado
O Protocolo utilizado pelo IRC é baseado em troca de mensagens constituídas em um conjunto de códigos composto por 8 bits, compondo um octeco, podendo chegar até 512.
Estas Mensagens são assíncronas e não existe garantia de repostas. O Protocolo dos servidores é o mesmo usado na relação entre clientes e servidores, porém existe restrições para a conexão cliente-servidor, algumas mensagens não são aceitas de cliente.<br>
## 3.2 Mensagens
O IRC consiste em até 3 partes principais: o prefixo, comando e parâmetros de comando, e eles são separados por um caractere de espaço de espaço ASCII(0x20) cada.
parâmetros de comando. 
* prefixo (opcional):<br>
    O prefixo é utilizado em algumas situações para indicar quem é o servidor ou cliente de origem da mensagem.
    Se o mesmo estiver em falta, a origem será a conexão a partir de qual foi recebido a mensagem. Não usados por                 clientes.<br> 
* comando:<br>
    É uma string ou conjunto de três números que indica um comando ou uma resposta numérica.<br>
* parâmetro de comando:<br>
Até 15 argumentos que podem ser enviados durante as mensagens. O último argumento se iniciado por ':' pode conter espaços.

Representação da BNF das mensagens utilizadas no IRC:<br>
    
message	::= [':' prefix SPACE] command params crlf<br>
prefix	::= servername | nick '!' user ['@' host]<br>
command	::= letter {letter} | number number number<br>
params	::= SPACE [':' trailing | middle params]<br>
middle	::= qualquer seqüência de octetos, não incluindo SPACE, <br>	        
                   NUL, CR ou LF, o primeiro caracter não pode ser ':' <br>
trailing	::= qualquer seqüência de octetos, sem NUL, CR ou LF><br>
SPACE		::= ' ' {' '}<br>
crlf		::= CR LF<br>
servername	::= host<br>
host		::= ver RFC 952 para detalhes em nomes de hosts permitos<br>
nick		::= letter { letter | number | special }<br>
user		::= nonwhite { nonwhite }<br>
letter	::= 'a' ... 'z' | 'A' ... 'Z'<br>
number	::= '0' ... '9'<br>
special	::= '-' | '[' | ']' | '\' | '`' | '^' | '{' | '}' <br>
nonwhite	::= qualquer caracter menos SPACE, NUL, CR e LF<br>

# 4. Mensagens Aceitas
## 4.1 Registro de Conexão
Os comandos descritos aqui são usados para registrar uma conexão com um servidor IRC como um usuário.

| Mensagem | Parametro | RFC | Mensagem de Erro|
| -------- | --------- | --------- | --- |
|PASS| password  |Comando utilizado para definir uma senha de conexão. [RFC PASS](https://tools.ietf.org/html/rfc2812#section-3.1.1)| 'Not enough parameters'; 'You may not reregister'|
|NICK| nickname  |Comando utilizado para definir um apelido ao usuário ou alterar um existente. [RFC NICK](https://tools.ietf.org/html/rfc2812#section-3.1.2)| No nickname given; 'Erroneus Nickname' ; 'Nickname is already in use'|
|USER | user, mode, unused, realname | Comando utilizado para especificar o nome do usuário, do host  e o nome real de um novo usuário. [RFC USER](https://tools.ietf.org/html/rfc2812#section-3.1.3)| 'Not enough parameters' ; 'You may not reregister'|
|MODE|nickname ( "+" / "-" )( "i" / "w" / "o" / "O" / "r" ) )|Comando utilizado para identificar o modo como os usuários irão ser visualizados entre si. é necessário ter nickname e user cadastrados [RFC MODE](href=https://tools.ietf.org/html/rfc2812#section-3.1.5).| 'No such channel' ; 'Not enough parameters' ; 'is unknown mode char to me' ; 'Cannot change mode for other users'|
|QUIT|message|Comando utilizado para encerrar a sessão com uma mensagem de saída. [RFC QUIT](https://tools.ietf.org/html/rfc2812#section-3.1.7)

## 4.2 Operações de Canal
Os comandos descritos aqui são usados para manipulação de canais, suas propriedades e seus conteúdos.


| Mensagem | Parametro | RFC | Mensagem de Erro|
| -------- | --------- | --------- | --- |
|JOIN |  ( channel *( "," channel )  key *( "," key ) |Comando utilizado para um usuário entrar em canais especifícos. [RFC JOIN](https://tools.ietf.org/html/rfc2812#section-3.1.2)| 'Not enough parameters' |
|PART| channel *( "," channel )  Part Message |Comando para sair de um canal. [RFC PART](https://tools.ietf.org/html/rfc2812#section-3.2.2)| 'No such channel' ; 'You\'re not on that channel' |
|TOPIC| channel, topic |Alterar ou visualizar um tópico de um canal. [RFC_TOPIC](https://tools.ietf.org/html/rfc2812#section-3.2.4)| 'Not enough parameters' ; 'You\'re not on that channel' ; 'Not enough parameters' ; 'No such channel' | 
|NAMES| channel *( "," channel ) target | Comando utilizado para listar todos os nicknames visíveis. [RFC_NAME](https://tools.ietf.org/html/rfc2812#section-3.2.5)| 'No such server' | 
|LIST |  channel *( "," channel ) target | Comando utilizado para listar canais e seus tópicos. [RFC_LIST](https://tools.ietf.org/html/rfc2812#section-3.2.6)| 'No such server' | 

## 4.3 Envio de Mensagens
Os comandos descritos aqui são responsáveis pela comunicação entre usuários e canais.

| Mensagem | Parametro | RFC | Mensagem de Erro|
| -------- | --------- | --------- | --- |
|PRIVMSG | destinario mensagem | Utilizado para o envio de mensagens privadas entre usuários e envio de mensagem para canais. [RFC Private messages](https://tools.ietf.org/html/rfc2812#section-3.3.1)| 'No recipient' ; 'No text to send' ; 'No such nick/channel' ; 'Cannot send to channel' |

## 4.4 Consulta e Comando do Servidor
Os comandos descritos aqui são usados para retornar informações sobre qualquer servidor que esteja conectado a rede

| Mensagem | Parametro | RFC | Mensagem de Erro|
| -------- | --------- | --------- | --- |
|MOTD| target | Utilizado para obter "Mensagem do Dia" do servidor atual. [RFC MOTD](https://tools.ietf.org/html/rfc2812#section-3.4.1)| 'MOTD File is missing'|

## 4.5 Consultas Baseadas no Usuário

Os comandos descritos aqui são responsáveis pela comunicação entre usuários e canais.

| Mensagem | Parametro | RFC | Mensagem de Erro|
| -------- | --------- | --------- | --- |
|WHO| mask "o"| Utilizado para consulta de informações solicitada por um cliente. [RFC WHO](https://tools.ietf.org/html/rfc2812#section-3.6.1)|  |



