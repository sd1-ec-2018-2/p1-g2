# Registro de Conexão





| CASO DE TESTE | Comando NICK                 |
| ------------- | :--------------------------- |
| ID            | CT-NICK                      |
| OBJETIVO      | Inserir nickname no servidor |
| PRÉ-CONDIÇÕES | Estar conectado              |

| ENTRADAS                                   |              |
| :----------------------------------------- | ------------ |
| 1- Comando nick passando apenas '*'        | nick *       |
| 2- Comando nick passando nickname único    | nick antonio |
| 3- Comando nick passando nickname repetido | nick antonio |

| SAÍDAS                       |                                       |
| :--------------------------- | ------------------------------------- |
| 1- ERR_ERRONEUSNICKNAME      | "* :Erroneous nickname"               |
| 2- Nenhuma resposta esperada |                                       |
| 3- ERR_NICKNAMEINUSE         | "antonio :Nickname is already in use" |





| CASO DE TESTE | Comando PASS                |
| ------------- | --------------------------- |
| ID            | CT-PASS                     |
| OBJETIVO      | Senha de acesso ao servidor |
| PRÉ-CONDIÇÕES |                             |

| ENTRADAS                                      |        |
| --------------------------------------------- | ------ |
| 1- Comando PASS passando parâmetro incompleto | PASS 0 |

| SAÍDAS                |                          |
| --------------------- | ------------------------ |
| 1- ERR_NEEDMOREPARAMS | ":Not enough parameters" |







| CASO DE TESTE | Comando MODE                   |
| :------------ | ------------------------------ |
| ID            | CT-MODE                        |
| OBJETIVO      | Alterar modo de operação       |
| PRÉ-CONDIÇÕES | Estar cadastrado (Nick e User) |

| ENTRADAS                                      |                       |
| --------------------------------------------- | --------------------- |
| 1- Comando MODE passando parâmetro incompleto | MODE                  |
| 2- Comando MODE passando canal inexistente    | MODE CanalInexistente |

| SAÍDAS                |                                     |
| --------------------- | ----------------------------------- |
| 1- ERR_NEEDMOREPARAMS | "Not enough parameters"             |
| 2- ERR_NOSUCHCHANNEL  | "CanalInexistente :No such channel" |







| CASO DE TESTE | Comando USER                 |
| ------------- | ---------------------------- |
| ID            | CT-USER                      |
| OBJETIVO      | Adcionar usuário ao servidor |
| PRÉ-CONDIÇÕES | Ter nickname cadastrado      |

| ENTRADAS                                      |                                 |
| --------------------------------------------- | ------------------------------- |
| 1- Comando USER passando parâmetro incompleto | USER                            |
| 2- Comando USER passandoparâmetro repetido    | USER antonio 0 *: Antonio Igor  |
| 3- Comando userpassando parâmetro inválido    | USER antonio a * : Antonio Igor |

| SAÍDAS                  |                                             |
| ----------------------- | ------------------------------------------- |
| 1- ERR_NEEDMOREPARAMS   | "Not enough parameters"                     |
| 2- ERR_ALREADYREGISTRED | "Unauthorized command (already registered)" |
| 3-ERR_NEEDMOREPARAMS    | "Not enough parameters"                     |







# Operações de Canal



| CASO DE TESTE | Comando JOIN                         |
| ------------- | ------------------------------------ |
| ID            | CT-JOIN                              |
| OBJETIVO      | Criar e entrar em canais do servidor |
| PRÉ-CONDIÇÕES | Estar cadastrado (Nick e User)       |

| ENTRADAS                                      |      |
| --------------------------------------------- | ---- |
| 1- Comando JOIN passando parâmetro incompleto | JOIN |

| SAÍDAS                |                               |
| --------------------- | ----------------------------- |
| 1- ERR_NEEDMOREPARAMS | "JOIN :Not enough parameters" |







| CASO DE TESTE | Comando PART                     |
| ------------- | -------------------------------- |
| ID            | CT-PART                          |
| OBJETIVO      | Sair de canais do servidor       |
| PRÉ-CONDIÇÕES | Estar em algum canal do servidor |

| ENTRADAS                                                     |                |
| ------------------------------------------------------------ | -------------- |
| 1- Comando PART passando parâmetro incompleto                | PART           |
| 2- Comando PART utilizado em um canal que o usuário não é membro | PART #SAOPAULO |
| 3- Comando PART passando canal inválido                      | PART #SAOPALO  |

| SAÍDAS                |                                        |
| --------------------- | -------------------------------------- |
| 1- ERR_NEEDMOREPARAMS | "Not enough parameters"                |
| 2- ERR_NOTONCHANNEL   | "SAOPAULO :You're not on that channel" |
| 3-ERR_NOSUCHCHANNEL   | "SAOPALO :No such channel"             |





| CASO DE TESTE | Comando TOPIC                                     |
| ------------- | ------------------------------------------------- |
| ID            | CT-TOPIC                                          |
| OBJETIVO      | Visualizar ou alterar tema do canal               |
| PRÉ-CONDIÇÕES | Cliente deve ter permissão (criador ou moderador) |

| ENTRADAS                                                     |                                 |
| ------------------------------------------------------------ | ------------------------------- |
| 1- Comando TOPIC passando parâmetro incompleto               | TOPIC                           |
| 2- Comando TOPIC utilizado em um canal que o usuário não é membro | TOPIC #SAOPAULO: CANAL PAULISTA |
| 3- Comando PART passando canal inválido ou inexistente       | TOPIC  SAOPALO                  |

| SAÍDAS                |                                        |
| --------------------- | -------------------------------------- |
| 1- ERR_NEEDMOREPARAMS | "Not enough parameters"                |
| 2- ERR_NOTONCHANNEL   | "SAOPAULO :You're not on that channel" |
| 3-ERR_NOSUCHCHANNEL   | "SAOPALO :No such channel"             |







# Envio de Mensagens



| CASO DE TESTE | Comando PRIVMSG                                              |
| ------------- | ------------------------------------------------------------ |
| ID            | CT-PRIVMSG                                                   |
| OBJETIVO      | Envio de mensagens privadas e envio de mensagens para canais |
| PRÉ-CONDIÇÕES | Cliente deve ter permissão (criador ou moderador)            |

| ENTRADAS                                                   |                            |
| ---------------------------------------------------------- | -------------------------- |
| 1-  Comando PRIVMSG sem parâmetros destinatário e mensagem | PRIVMSG                    |
| 2- Comando PRIVMS para nickname invalido ou não cadastrado | PRIVMSG naoCadastrado: ola |

| SAÍDAS             |                                       |
| ------------------ | ------------------------------------- |
| 1- ERR_NORECIPIENT | "teste :'No recipient given "         |
| 2- ERR_NOSUCHNICK  | "naoCadastrado :No such nick/channel" |





# Consultas ao Servidor



| CASO DE TESTE | Comando MOTD                                                |
| ------------- | ----------------------------------------------------------- |
| ID            | CT-MOTD                                                     |
| OBJETIVO      | Consultar "Mensagem do Dia"                                 |
| PRÉ-CONDIÇÕES | Cliente estar registrado em um servidor e estar em um canal |

| ENTRADAS                              |      |
| ------------------------------------- | ---- |
| 1- Comando MOTD porém não há mensagem | MOTD |

| SAÍDAS        |                                  |
| ------------- | -------------------------------- |
| 1- ERR_NOMOTD | ":antonio :MOTD File is missing" |





# Variadas



| CASO DE TESTE | Comando PING                   |
| ------------- | ------------------------------ |
| ID            | CT-PING                        |
| OBJETIVO      | Testar se o usuário esta ativo |
| PRÉ-CONDIÇÕES | Estar conectado no servidor    |

| ENTRADAS                      |      |
| ----------------------------- | ---- |
| 1- Comando PING sem parâmetro | PING |

| SAÍDAS                |                              |
| --------------------- | ---------------------------- |
| 1- ERR_NEEDMOREPARAMS | "PING:Not enough parameters" |





| CASO DE TESTE | Comando PONG                   |
| ------------- | ------------------------------ |
| ID            | CT-PONG                        |
| OBJETIVO      | Testar se o usuário esta ativo |
| PRÉ-CONDIÇÕES | Estar conectado no servidor    |

| ENTRADAS              |      |
| --------------------- | ---- |
| 1- Comando PONG VAZIO | PONG |

| SAÍDAS                |                              |
| --------------------- | ---------------------------- |
| 1- ERR_NEEDMOREPARAMS | "PONG:Not enough parameters" |





| CASO DE TESTE | Comando KILL        |
| ------------- | ------------------- |
| ID            | CT-KILL             |
| OBJETIVO      | Expulsar um cliente |
| PRÉ-CONDIÇÕES |                     |

| ENTRADAS                           |      |
| ---------------------------------- | ---- |
| 1- Comando KILL com sem parâmetros | KILL |

| SAÍDAS                |                              |
| --------------------- | ---------------------------- |
| 1- ERR_NEEDMOREPARAMS | "KILL:Not enough parameters" |