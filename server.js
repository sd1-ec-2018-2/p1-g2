const net = require('net');
const ClientList = require('./model/client_list.js');
const ChannelList = require('./model/channel_list.js');
//Importa o principal objeto que irá interpretar as resposta do usuario e exercutar as ações necessárias
const irc = require('./irc/irc.js');

//Porta em que o servidor é inciado
const PORT = 6667;

//Invoca a classe ClientList que lida com o armazenamento dos sockets e informações relevantes dos clientes conectados
const clientList = new ClientList();
const channelList = new ChannelList();

net.createServer(function(socket) {
    //Configura o encoding das mensagens recebidas e transmitidas pelo servidor,
    //transforma automaticamente os dados recebidos em string
    socket.setEncoding('utf8');

    //A cada nova conexão adiciona um novo client a lista
    var client = clientList.addClient(socket);
    irc.serverWriteMsg('Bem vindo ao servidor IRC do Grupo 2', client, 'NOTICE')

    //Função responsavel por lidar com o que acontece após o cliente se enviar dados ao servidor
    socket.on('data', function(data){
        //Interpreta a mensagem do enviada pelo cliente e manda a resposta adequada
        irc.listen(data, client, clientList, channelList);
        console.log(data);
    });

    //Função responsavel por lidar com o que acontece após o cliente se desconectar
    socket.on('end', function(){
        //Mantem um historico dos canais para mandar a mensagens de saida
        var old_channel_list = client.channelList.slice()
        //Remove o cliente desconectado da lista de clientes
        clientList.removeClient(client.id);
        //Remove o cliente de todos os canais
        channelList.removeClientFromAll(client.id);

        if(!client.quitted){
            old_channel_list.forEach((channel) => {
                channel.broadcast(`:${client.nick}!${client.username}@${client.iplocal} QUIT :Remote host closed the connection`);
            });
        }

        console.log('Cliente de id: '+client.id+' saiu!');
    });

}).listen(PORT);

console.log('Servidor IRC iniciado na porta: ' + PORT);


// Faz a verificação se o usuário está ativo enviado o comando PING e esperando o comando PONG como resposta em intervalos de tempo
setInterval(function () {
    clientList._list.forEach(client => {
        // Se o cliente não respondeu ao último PING, derrubamos ele e passamos aõ próximo cliente
        if(client.active === false){
            client.write(`You are being disconneted by ${irc.servername} because of INACTIVITY (NOT REPLIED SERVER PING)`);
            client.socket.end()
            return
        }
        //Enviar o comando PING
        client.active = false;
        client.write(`PING ${irc.servername} : ${irc.servername}`);
    });
}, 60000); //Intervalor de tempo da checagem de inatividade em milisegundos
